%{
	//#include "yacc.tab.h"
	#include <stdio.h>
	#include <string.h>

	#define MAX_TIEMS 20

	//if C++, user extern C structure
	int K_count = 0;
	int I_count = 0;
	int C_count = 0;
	int P_count = 0;
	int O_count = 0;

	struct dict
	{
		char element[5];
		int count;
	};

	struct type_detail
	{
		struct dict dictionarys[MAX_TIEMS];
		int count;
	};

	struct type_detail types[5];

int add(int type_num, char* key_word) {
	int j;
	if(strlen(key_word)>MAX_TIEMS)
	{
		return 0;
	}

	for(j=0;j<MAX_TIEMS;j++)
	{		
		if(strcmp(types[type_num].dictionarys[j].element,key_word)==0)
		{
			types[type_num].dictionarys[j].count++;
			break;
		}
		if(strcmp(types[type_num].dictionarys[j].element,"NULL")==0)
		{
			strncpy(types[type_num].dictionarys[j].element,key_word,4);
			types[type_num].dictionarys[j].element[4] = '\0';
			types[type_num].dictionarys[j].count++;
			types[type_num].count++;
			break;
		}
	}
	return 0;
}

%}

K	"procedure"|"function"|"begin"|"end"|"read"|"write"|"if"|"call"|"repeat"|"then"|"until"|"var"
I 	[a-zA-Z]*
C 	[0-9]*
P	","|";"|":="|"+"|"-"|"*"|"/"|"."|"="|"("|")"|"<"|">"
O	.

%%

{K}		{K_count++;add(0,yytext);}
{I}		{I_count++;add(1,yytext);}
{C}		{C_count++;add(2,yytext);}
{P} 	{P_count++;add(3,yytext);}
{O}		{O_count++;add(4,yytext);}
%%

int printOut()
{
	int i,j;
	for(i=0;i<5;i++)
	{
		printf("Type:\n");
		for(j=0;j<MAX_TIEMS;j++)
		{
			if(strcmp(types[i].dictionarys[j].element,"NULL")==0)
			{
				break;
			}
			printf("####%s in total:%d#####\n",types[i].dictionarys[j].element,types[i].dictionarys[j].count);
		}
		printf("\n");
	}
}

int main() {

	int i,j;

	for (int i = 0; i < 5; ++i)
	{
		types[i].count = 0;
		for (int j = 0; j < MAX_TIEMS; ++j)
		{
			types[i].dictionarys[j].element[0] = 'N';
			types[i].dictionarys[j].element[1] = 'U';
			types[i].dictionarys[j].element[2] = 'L';
			types[i].dictionarys[j].element[3] = 'L';
			types[i].dictionarys[j].element[4] = '\0';
			types[i].dictionarys[j].count = 0;
		}
	}

	yylex();
	printf("Type K conut: \t %d\n",K_count );
	printf("Type I conut: \t %d\n",I_count );
	printf("Type C conut: \t %d\n",C_count );
	printf("Type P conut: \t %d\n",P_count );
	printf("Type O conut: \t %d\n",O_count );

	printf("Details\n" );
	printOut();
	return 0;
}