/* A Bison parser, made from PLtoPC.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

# define	IF	257
# define	THEN	258
# define	ELSE	259
# define	WHILE	260
# define	DO	261
# define	READ	262
# define	WRITE	263
# define	CALL	264
# define	BEGIN	265
# define	END	266
# define	CONST	267
# define	VAR	268
# define	PROCEDURE	269
# define	ODD	270
# define	INTEGER	271
# define	VARIABLE	272
# define	EQUAL	273
# define	NL	274
# define	NG	275
# define	REPEAT	276
# define	UNTIL	277
# define	SEMICOLON	278
# define	UMINUS	279
# define	LOWER_THAN_ELSE	280

#line 1 "PLtoPC.y"

	#include <stdlib.h>
	#include <stdio.h>
	#include <malloc.h> 
	#include <memory.h>
	#include <string.h>
	#include "PLtoPC.tab.h"
	
	int yylex (void); 
	void yyerror(char *str);
	int find(int type);
	int check ();
	void gen(int f,int l,int a);
	void AddSymbol(int type, int value);
//	void AddArray(int begin,int end);
	extern FILE *yyin;
	extern char* yytext;
	extern int yylval;
	FILE *fIn,*FOut;
	
	int disp[DISPSIZE]; //display表
	int offset[SYMBOLSIZE]; //为当前层分配的变量空间	
	int name[NAMESIZE]; //储存当前变量名；
	int CodeNo; //当前指令条数
	int PreLevel; //当前所在层次
	int Top; //符号栈栈顶
	int Pos; //元素的在符号表中的位置（配合find()）
	int BackStack[CODESIZE]; //回填栈,用于过程的条件跳转位置的回填
	int Back; //记录BackStack回填位置
	int BackStack1[CODESIZE]; //回填栈,用于if的条件跳转位置的回填
	int Back1; //记录BackStack1回填位置
	int BackStack2[CODESIZE]; //回填栈,用于then部分编译结束而else尚未编译的时的无条件跳转位置的回填
	int Back2; //记录BackStack2回填位置
	int BackStack3[CODESIZE]; //回填栈,用于whiledo循环的无条件跳转位置的回填及util的条件跳转的回填
	int Back3; //记录BackStack3回填位置
	int l,a; //变量的偏移量和层差，主要用于赋值语句中事先记录左边变量的偏移量和层差，让yytext扫完后面的表达式后不致移回致使死循环

	typedef struct instruction{
		int f; //功能码
		int l; //层次差
		int a; //因指令而异
	}instruction;
	struct instruction code[CODESIZE]; //指令表

	typedef struct Symbols{
		int type; //定义的符号表中的类，取值为TCONST，TVAR，TPROC，TARRAY
		char name[NAMESIZE]; //常量或变量名
		int value; //常量或变量的值
		int level; //变量所在层次
		int offset; //偏移量，一般为3，0为静态链，1为动态链，2为返回地址
		int entry; //过程入口
	}Symbols;
	struct Symbols symbols[SYMBOLSIZE]; //符号表
	
#ifndef YYSTYPE
# define YYSTYPE int
# define YYSTYPE_IS_TRIVIAL 1
#endif
#ifndef YYDEBUG
# define YYDEBUG 0
#endif



#define	YYFINAL		137
#define	YYFLAG		-32768
#define	YYNTBASE	39

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 280 ? yytranslate[x] : 79)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,    33,     2,     2,     2,     2,
      37,    38,    27,    25,    32,    26,    31,    28,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      35,    36,    34,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    29,
      30
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     4,    12,    16,    17,    21,    27,    28,    32,
      33,    35,    39,    41,    45,    48,    49,    53,    59,    67,
      69,    71,    73,    75,    77,    79,    81,    83,    87,    92,
      96,   100,   104,   108,   112,   116,   120,   123,   130,   141,
     143,   147,   151,   154,   157,   159,   163,   165,   169,   173,
     175,   177,   181,   190,   197,   200,   205,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224
};
static const short yyrhs[] =
{
      64,    40,    31,     0,    65,    41,    43,    47,    67,    66,
      50,     0,    13,    42,    24,     0,     0,    18,    19,    17,
       0,    42,    32,    18,    19,    17,     0,     0,    14,    44,
      24,     0,     0,    45,     0,    44,    32,    45,     0,    18,
       0,    15,    18,    24,     0,    48,    24,     0,     0,    46,
      69,    49,     0,    48,    24,    46,    69,    49,     0,    41,
      43,    47,    70,    66,    50,    71,     0,    52,     0,    53,
       0,    55,     0,    60,     0,    61,     0,    62,     0,    63,
       0,    50,     0,    51,    24,    50,     0,    18,    68,    19,
      56,     0,    11,    51,    12,     0,    56,    33,    56,     0,
      56,    34,    56,     0,    56,    35,    56,     0,    56,    21,
      56,     0,    56,    20,    56,     0,    56,    36,    56,     0,
      16,    56,     0,     3,    54,    72,     4,    50,    73,     0,
       3,    54,    72,     4,    50,     5,    74,    73,    50,    75,
       0,    58,     0,    56,    25,    56,     0,    56,    26,    56,
       0,    26,    56,     0,    25,    56,     0,    56,     0,    57,
      32,    56,     0,    59,     0,    58,    27,    58,     0,    58,
      28,    58,     0,    17,     0,    18,     0,    37,    56,    38,
       0,     6,    78,    54,    72,     7,    50,    76,    73,     0,
      22,    78,    51,    23,    54,    77,     0,    10,    18,     0,
       8,    37,    18,    38,     0,     9,    37,    57,    38,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,    88,    92,    96,    97,   101,   102,   103,   107,   108,
     112,   113,   117,   122,   130,   131,   135,   136,   140,   144,
     145,   146,   147,   148,   149,   150,   155,   156,   160,   164,
     168,   169,   170,   171,   172,   173,   174,   178,   179,   183,
     184,   185,   186,   187,   191,   192,   196,   197,   198,   202,
     203,   212,   216,   217,   221,   228,   236,   242,   255,   259,
     263,   267,   275,   279,   283,   291,   295,   299,   302,   306,
     310,   314
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "IF", "THEN", "ELSE", "WHILE", "DO", "READ", 
  "WRITE", "CALL", "BEGIN", "END", "CONST", "VAR", "PROCEDURE", "ODD", 
  "INTEGER", "VARIABLE", "EQUAL", "NL", "NG", "REPEAT", "UNTIL", 
  "SEMICOLON", "'+'", "'-'", "'*'", "'/'", "UMINUS", "LOWER_THAN_ELSE", 
  "'.'", "','", "'#'", "'>'", "'<'", "'='", "'('", "')'", "Program", 
  "Block", "ConstantDeclaration", "ConstantStatement", 
  "VariableDeclaration", "VariableStatement", "Identifier", 
  "ProcedureHeader", "ProcedureDeclaration", "ProcedureStatement", 
  "ProBlock", "Statement", "Statements", "AssignmentStatement", 
  "CompoundStatement", "Condition", "ConditionalStatement", "Expression", 
  "Expressions", "Term", "Factor", "WhileDoStatement", "CallStatement", 
  "ReadStatement", "WriteStatement", "Init", "SetDefaultOffset", 
  "InitOffset", "BackfillBlock", "GetAssign", "GenCodeProc1", "Backfill", 
  "GenCodeProc2", "GenCode1", "Backfill1", "GenCode2", "Backfill2", 
  "GenCodeWhile", "GenCodeUntil", "Backfill3", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    39,    40,    41,    41,    42,    42,    42,    43,    43,
      44,    44,    45,    46,    47,    47,    48,    48,    49,    50,
      50,    50,    50,    50,    50,    50,    51,    51,    52,    53,
      54,    54,    54,    54,    54,    54,    54,    55,    55,    56,
      56,    56,    56,    56,    57,    57,    58,    58,    58,    59,
      59,    59,    60,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     3,     7,     3,     0,     3,     5,     0,     3,     0,
       1,     3,     1,     3,     2,     0,     3,     5,     7,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     4,     3,
       3,     3,     3,     3,     3,     3,     2,     6,    10,     1,
       3,     3,     2,     2,     1,     3,     1,     3,     3,     1,
       1,     3,     8,     6,     2,     4,     4,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
      57,    58,     0,     4,     1,     7,     9,     0,     0,     0,
      15,     0,     3,     0,    12,     0,    10,     0,    62,    60,
       0,     5,     0,     8,     0,     0,     4,    59,    14,     0,
      11,    13,     9,    16,     0,    62,     6,    15,     0,    71,
       0,     0,     0,     0,    61,    71,     2,    19,    20,    21,
      22,    23,    24,    25,     4,    63,     0,    49,    50,     0,
       0,     0,    65,     0,    39,    46,     0,     0,     0,    54,
      26,     0,     0,     0,    17,    59,    36,    43,    42,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    65,     0,    44,     0,    29,     0,     0,     0,     0,
      51,     0,    34,    33,    40,    41,    30,    31,    32,    35,
      47,    48,     0,    55,     0,    56,    27,    28,     0,    64,
      66,     0,    45,    70,    18,    67,    37,    69,    53,    66,
      66,     0,    52,    68,    38,     0,     0,     0
};

static const short yydefgoto[] =
{
     135,     2,    32,     8,    10,    15,    16,    18,    19,    20,
      33,    70,    71,    47,    48,    62,    49,    63,    94,    64,
      65,    50,    51,    52,    53,     1,     3,    34,    27,    72,
      26,    75,   124,    80,   126,   129,   134,   130,   128,    66
};

static const short yypact[] =
{
  -32768,-32768,   -19,     9,-32768,    -8,    25,    22,   -21,    35,
      31,    26,-32768,    37,-32768,    -7,-32768,    46,-32768,-32768,
      48,-32768,    54,-32768,    35,    50,     9,-32768,    31,    62,
  -32768,-32768,    25,-32768,    60,-32768,-32768,    31,    -2,-32768,
      40,    47,    65,    60,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,-32768,-32768,-32768,     9,-32768,    19,-32768,-32768,    19,
      19,    19,-32768,    55,    20,-32768,    -2,    67,    19,-32768,
  -32768,    -3,    73,    60,-32768,-32768,    24,-32768,-32768,   -18,
      82,    19,    19,    19,    19,    19,    19,    19,    19,     1,
       1,-32768,    56,    24,     2,-32768,    60,    19,    28,    60,
  -32768,    60,    24,    24,-32768,-32768,    24,    24,    24,    24,
  -32768,-32768,    86,-32768,    19,-32768,-32768,    24,    -2,-32768,
      90,    60,    24,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
  -32768,    60,-32768,-32768,-32768,    96,    98,-32768
};

static const short yypgoto[] =
{
  -32768,-32768,    97,-32768,    69,-32768,    75,    74,    66,-32768,
      51,   -34,    33,-32768,-32768,   -64,-32768,   -55,-32768,   -32,
  -32768,-32768,-32768,-32768,-32768,-32768,-32768,    29,-32768,-32768,
      72,-32768,-32768,    17,   -69,-32768,-32768,-32768,-32768,    64
};


#define	YYLAST		109


static const short yytable[] =
{
      46,    76,    91,    12,    77,    78,    79,    83,    84,    95,
       7,    13,     4,    93,    56,    57,    58,    23,    57,    58,
     100,    96,     5,    59,    60,    24,   102,   103,   104,   105,
     106,   107,   108,   109,   114,    61,    57,    58,    61,     9,
     115,    11,   117,    21,    59,    60,    17,    89,    90,    83,
      84,   118,    96,    14,   123,    22,    61,   110,   111,   122,
     131,   132,   116,    38,    25,   119,    39,   120,    40,    41,
      42,    43,    28,    29,    31,    81,    82,    67,    44,    36,
      83,    84,    45,    69,    68,    92,   101,   127,    85,    86,
      87,    88,    97,   121,   113,   125,   136,   133,   137,    30,
       6,    37,    35,    55,    99,    74,    98,    54,   112,    73
};

static const short yycheck[] =
{
      34,    56,    66,    24,    59,    60,    61,    25,    26,    12,
      18,    32,    31,    68,    16,    17,    18,    24,    17,    18,
      38,    24,    13,    25,    26,    32,    81,    82,    83,    84,
      85,    86,    87,    88,    32,    37,    17,    18,    37,    14,
      38,    19,    97,    17,    25,    26,    15,    27,    28,    25,
      26,    23,    24,    18,   118,    18,    37,    89,    90,   114,
     129,   130,    96,     3,    18,    99,     6,   101,     8,     9,
      10,    11,    24,    19,    24,    20,    21,    37,    18,    17,
      25,    26,    22,    18,    37,    18,     4,   121,    33,    34,
      35,    36,    19,     7,    38,     5,     0,   131,     0,    24,
       3,    32,    28,    37,    75,    54,    73,    35,    91,    45
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

#line 315 "bison.simple"


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 1:
#line 88 "PLtoPC.y"
{ gen(OPR,0,0); ;
    break;}
case 2:
#line 92 "PLtoPC.y"
{  ;
    break;}
case 3:
#line 96 "PLtoPC.y"
{  ;
    break;}
case 5:
#line 101 "PLtoPC.y"
{ AddSymbol(TCONST, yyvsp[0]);;
    break;}
case 6:
#line 102 "PLtoPC.y"
{ AddSymbol(TCONST, yyvsp[0]); ;
    break;}
case 8:
#line 107 "PLtoPC.y"
{ ;
    break;}
case 10:
#line 112 "PLtoPC.y"
{  ;
    break;}
case 11:
#line 113 "PLtoPC.y"
{  ;
    break;}
case 12:
#line 117 "PLtoPC.y"
{ AddSymbol(TVAR,-1); ;
    break;}
case 13:
#line 122 "PLtoPC.y"
{ 
				AddSymbol(TPROC,-1); //过程变量不需要赋值，故value设为-1
				disp[++PreLevel] = Top;
				offset[PreLevel] = 3;
			;
    break;}
case 14:
#line 130 "PLtoPC.y"
{ ;
    break;}
case 16:
#line 135 "PLtoPC.y"
{ ;
    break;}
case 17:
#line 136 "PLtoPC.y"
{ ;
    break;}
case 18:
#line 140 "PLtoPC.y"
{;
    break;}
case 19:
#line 144 "PLtoPC.y"
{  ;
    break;}
case 20:
#line 145 "PLtoPC.y"
{  ;
    break;}
case 21:
#line 146 "PLtoPC.y"
{  ;
    break;}
case 22:
#line 147 "PLtoPC.y"
{  ;
    break;}
case 23:
#line 148 "PLtoPC.y"
{  ;
    break;}
case 24:
#line 149 "PLtoPC.y"
{  ;
    break;}
case 25:
#line 150 "PLtoPC.y"
{  ;
    break;}
case 26:
#line 155 "PLtoPC.y"
{  ;
    break;}
case 27:
#line 156 "PLtoPC.y"
{  ;
    break;}
case 28:
#line 160 "PLtoPC.y"
{ gen(STO,l,a);;
    break;}
case 29:
#line 164 "PLtoPC.y"
{ ;
    break;}
case 30:
#line 168 "PLtoPC.y"
{ gen(OPR,0,9); ;
    break;}
case 31:
#line 169 "PLtoPC.y"
{ gen(OPR,0,12); ;
    break;}
case 32:
#line 170 "PLtoPC.y"
{ gen(OPR,0,10); ;
    break;}
case 33:
#line 171 "PLtoPC.y"
{ gen(OPR,0,13); ;
    break;}
case 34:
#line 172 "PLtoPC.y"
{ gen(OPR,0,11); ;
    break;}
case 35:
#line 173 "PLtoPC.y"
{ gen(OPR,0,8); ;
    break;}
case 36:
#line 174 "PLtoPC.y"
{ gen(OPR,0,6); ;
    break;}
case 37:
#line 178 "PLtoPC.y"
{  ;
    break;}
case 38:
#line 179 "PLtoPC.y"
{  ;
    break;}
case 39:
#line 183 "PLtoPC.y"
{  ;
    break;}
case 40:
#line 184 "PLtoPC.y"
{ gen(OPR,0,2); ;
    break;}
case 41:
#line 185 "PLtoPC.y"
{ gen(OPR,0,3); ;
    break;}
case 42:
#line 186 "PLtoPC.y"
{ gen(OPR,0,1); ;
    break;}
case 43:
#line 187 "PLtoPC.y"
{  ;
    break;}
case 44:
#line 191 "PLtoPC.y"
{ ;
    break;}
case 45:
#line 192 "PLtoPC.y"
{  ;
    break;}
case 46:
#line 196 "PLtoPC.y"
{  ;
    break;}
case 47:
#line 197 "PLtoPC.y"
{ gen(OPR,0,4); ;
    break;}
case 48:
#line 198 "PLtoPC.y"
{ gen(OPR,0,5); ;
    break;}
case 49:
#line 202 "PLtoPC.y"
{ gen(LIT,0,yyvsp[0]); ;
    break;}
case 50:
#line 203 "PLtoPC.y"
{ 
				Pos = find(TVAR);
				if (Pos>=0)
					gen(LOD,PreLevel - symbols[Pos].level,symbols[Pos].offset);
				else {
						Pos = find(TCONST);
						gen(LIT,0,symbols[Pos].value);
				} 
			;
    break;}
case 51:
#line 212 "PLtoPC.y"
{  ;
    break;}
case 52:
#line 216 "PLtoPC.y"
{  ;
    break;}
case 53:
#line 217 "PLtoPC.y"
{  ;
    break;}
case 54:
#line 221 "PLtoPC.y"
{ 
				Pos = find(TPROC);
				gen(CAL,PreLevel-symbols[Pos].level,symbols[Pos].entry);
				;
    break;}
case 55:
#line 228 "PLtoPC.y"
{ 
				gen(OPR,0,16);
				Pos = find(TVAR);
				gen(STO,PreLevel - symbols[Pos].level,symbols[Pos].offset);
			;
    break;}
case 56:
#line 236 "PLtoPC.y"
{ gen(OPR,0,14);gen(OPR,0,15);;
    break;}
case 57:
#line 242 "PLtoPC.y"
{
				CodeNo=0;
				PreLevel=0;
				Top=0;
				disp[PreLevel]=0;
				gen(JMP,0,0);
				Back1=0;
				Back2=0;
				Back3=0;
			;
    break;}
case 58:
#line 255 "PLtoPC.y"
{ offset[PreLevel] = 3; ;
    break;}
case 59:
#line 259 "PLtoPC.y"
{ gen(INT,0,offset[PreLevel]); ;
    break;}
case 60:
#line 263 "PLtoPC.y"
{ code[0].a = CodeNo; ;
    break;}
case 61:
#line 267 "PLtoPC.y"
{ 
				Pos = find(TVAR);
				l = PreLevel - symbols[Pos].level;
				a = symbols[Pos].offset; 
			;
    break;}
case 62:
#line 275 "PLtoPC.y"
{gen(JMP,0,0); BackStack[Back++] = CodeNo-1;;
    break;}
case 63:
#line 279 "PLtoPC.y"
{ code[BackStack[--Back]].a = CodeNo; ;
    break;}
case 64:
#line 283 "PLtoPC.y"
{
				gen(OPR,0,0);
				Top = disp[PreLevel];
				PreLevel--;
			;
    break;}
case 65:
#line 291 "PLtoPC.y"
{ gen(JPC,0,0); BackStack1[Back1++] = CodeNo-1; ;
    break;}
case 66:
#line 295 "PLtoPC.y"
{ code[BackStack1[--Back1]].a = CodeNo; ;
    break;}
case 67:
#line 299 "PLtoPC.y"
{ gen(JMP,0,0); BackStack2[Back2++] = CodeNo-1; ;
    break;}
case 68:
#line 302 "PLtoPC.y"
{ code[BackStack2[--Back2]].a = CodeNo; ;
    break;}
case 69:
#line 306 "PLtoPC.y"
{ gen(JMP,0,BackStack3[--Back3]); ;
    break;}
case 70:
#line 310 "PLtoPC.y"
{ gen(JPC,0,BackStack3[--Back3]); ;
    break;}
case 71:
#line 314 "PLtoPC.y"
{ BackStack3[Back3++] = CodeNo;;
    break;}
}

#line 705 "bison.simple"


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
#line 317 "PLtoPC.y"


void yyerror( char * s ){
	printf( "parse error" );
}

void gen (int f,int l, int a) {/*产生三元式目标代码*/
	code[CodeNo].f = f;
	code[CodeNo].l = l;
	code[CodeNo].a = a;
	CodeNo++;
}

int find(int type){ /*在符号表中寻找元素的位置*/
	int l = PreLevel, i = Top-1;
	while (l >= 0){
		while (i >= disp[l]){
			if (strcmp(symbols[i].name,name)==0 && symbols[i].type==type)
				return i;
			i--;
		}
		l--;
	}
	return -1;
}

int check () { /*检查变量在符号表中是否重复*/
	int i;
	for(i=disp[PreLevel];i<Top;i++) {
		if(strcmp(symbols[i].name,name)==0)
		{
			return i;
		}
	}
	return -1;
}

void AddSymbol(int type, int value) { /*将非数组变量登入符号表*/
	if (check() != -1)
		printf("Error : Redefined!\n");
	switch(type) {
		case TCONST: { //常量
			strcpy(symbols[Top].name,name);
			symbols[Top].type = type;
			symbols[Top].value = value;
			Top++;
			break;
			}
		case TVAR: { //变量
			strcpy(symbols[Top].name,name);
			symbols[Top].type = type;
			symbols[Top].offset = offset[PreLevel];
			symbols[Top].level = PreLevel;
			offset[PreLevel]++;
			Top++;
			break;
			}
		case TPROC: { //过程
			strcpy(symbols[Top].name,name);
			symbols[Top].type = type;
			symbols[Top].level = PreLevel;
			symbols[Top].entry = CodeNo;
			Top++;
			break;
			}
		default:break;
	}
}

/*将非数组变量登入符号表，由于便于过程无法计算下表变量的值，
无法确定其在符号栈的位置，故PCODE无法实现，暂时略去*/
void AddArray(int begin,int end) { 
	/*if (check() != -1) {
		printf("Error : Redifined!\n");
		return;
	}
	}*/
}

int base(int l,int* s,int b) { //需找当前层的基址，对应interpret中的函数
	//l表示层差，s表示运行栈，b表示当前层基址
	if (l<0){
		printf("Error : Unknown Base!\n");
		return -1;
	}
	while (l>0){ //若l>0,顺着静态链往回找
		b = s[b];
		l--;
	}
	if(l==0)
		return b;
}

void fprintcode() { //将输出代码写入文件
	int i;
	for (i=0;i<CodeNo;i++){
		switch (code[i].f){
			case LIT: fprintf(FOut,"LIT ");break;
			case LOD: fprintf(FOut,"LOD ");break;
			case STO: fprintf(FOut,"STO ");break;
			case CAL: fprintf(FOut,"CAL ");break;
			case INT: fprintf(FOut,"INT ");break;
			case JMP: fprintf(FOut,"JMP ");break;
			case JPC: fprintf(FOut,"JPC ");break;
			case OPR: fprintf(FOut,"OPR ");break;
			default:break;
		}
		fprintf(FOut,"%d %d\n",code[i].l,code[i].a);
	}
}

void interpret() 
{
    int p, b, t;
    instruction i;  //instruction （指令）的类型定义（包括三个域f,l,a），请自行加入到头文件中，供其他文件共享
    int s[STACKSIZE];//stacksize：数据栈大小，需自己定义

    printf("start pl0\n");
    t=0; b=0;  //t：数据栈顶指针；b：基地址；
    p=0;	// 指令指针
    s[1]=0; s[2]=0; s[3]=0;
    do {
	
        i=code[p++];//code为指令存放数组，其定义请自行加入到头文件中，供其他文件共享
        switch (i.f) 
        {
        case LIT: 
            t=t+1;
            s[t]=i.a;
            break;
        case OPR: 
            switch(i.a) 
            {
                case 0:
                    t=b-1;
                    p=s[t+3];
                    b=s[t+2];
                    break;
                case 1: 
                    s[t]=-s[t];
                    break;
                case 2: 
                    t=t-1;
                    s[t]=s[t] + s[t+1];
                    break;
                case 3:
                    t=t-1;
                    s[t]=s[t] - s[t+1];
                    break;
                case 4: 
                    t=t-1;
                    s[t]=s[t] * s[t+1];
                    break;
                case 5: 
                    t=t-1;
                    s[t]=s[t] / s[t+1];
                    break;
                case 6: 
                    s[t]=(s[t] % 2 == 1);
                    break;
                case 8: 
                    t=t-1;
					
                    s[t]=(s[t] == s[t+1]);
                    break;
                case 9:
                    t=t-1;
                    s[t]=(s[t] != s[t+1]);
                    break;
                case 10:
                    t=t-1;
                    s[t]=(s[t]<s[t+1]);
                    break;
                case 11: 
                    t=t-1;
                    s[t]=(s[t]>=s[t+1]);
                    break;
                case 12: 
                    t=t-1;
                    s[t]=(s[t]>s[t+1]);
                    break;
                case 13: 
                    t=t-1;
                    s[t]=(s[t]<=s[t+1]);
                    break;
                case 14: 
                    printf(" %d", s[t]);
                    t=t-1;
                    break;
                case 15: 
                    printf("\n");
                    break;
                case 16: 
                    t=t+1;
                    printf(" ? ");
                    scanf("%d", &s[t]);
                    break;
            }
		    break;
        case LOD: 
            t=t+1;
            s[t]=s[base(i.l, s, b)+i.a];
            break;
        case STO: 
            s[base(i.l, s, b)+i.a]=s[t];
            t=t-1;
            break;
        case CAL:
            s[t+1]=base(i.l, s, b);
            s[t+2]=b;
            s[t+3]=p;
            b=t+1;
            p=i.a;
            break;
        case INT: 
            t=t+i.a;
            break;
        case JMP: 
            p=i.a;
            break;
        case JPC: 
            if (s[t]==0) 
            {
                p=i.a;
            }
			//printf("---nextp %d  ----\n",p);
            t=t-1;
            break;
        }//end switch
    }while (p!=0);
}

int main ( int argc, char *argv[] )
{
	int i;
	char output[100];
	switch(argc)
	{
	case 1: break;		
	case 2: 			//打开指定文件
		if ( ( fIn = fopen( argv[ 1 ], "r") ) == NULL ) {
	            printf( "File %s not found.\n", argv[ 1 ] );
	            exit( 1 );
	        }
	        else {
			yyin=fIn;
			strcpy(output,argv[1]);
			output[strlen(output)-4]='\0';
			strcat(output,".opcode");
			}
		break;
	default:
		printf("useage:flex [filename]\n");
		exit(1);
	}

	FOut = fopen(output,"w");
//	yylex();             //调用yylex()函数，进行词法分析
	
	yyparse();
	fprintcode(FOut);
	fclose(fIn);
	fclose(FOut);
	interpret();
	return 0;
}
