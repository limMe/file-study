%{
	#include <stdlib.h>
	#include <stdio.h>
	#include <malloc.h> 
	#include <memory.h>
	#include <string.h>
	#include "PLtoPC.tab.h"
	
	int yylex (void); 
	void yyerror(char *str);
	int find(int type);
	int check ();
	void gen(int f,int l,int a);
	void AddSymbol(int type, int value);
//	void AddArray(int begin,int end);
	extern FILE *yyin;
	extern char* yytext;
	extern int yylval;
	FILE *fIn,*FOut;
	
	int disp[DISPSIZE]; //display表
	int offset[SYMBOLSIZE]; //为当前层分配的变量空间	
	int name[NAMESIZE]; //储存当前变量名；
	int CodeNo; //当前指令条数
	int PreLevel; //当前所在层次
	int Top; //符号栈栈顶
	int Pos; //元素的在符号表中的位置（配合find()）
	int BackStack[CODESIZE]; //回填栈,用于过程的条件跳转位置的回填
	int Back; //记录BackStack回填位置
	int BackStack1[CODESIZE]; //回填栈,用于if的条件跳转位置的回填
	int Back1; //记录BackStack1回填位置
	int BackStack2[CODESIZE]; //回填栈,用于then部分编译结束而else尚未编译的时的无条件跳转位置的回填
	int Back2; //记录BackStack2回填位置
	int BackStack3[CODESIZE]; //回填栈,用于whiledo循环的无条件跳转位置的回填及util的条件跳转的回填
	int Back3; //记录BackStack3回填位置
	int l,a; //变量的偏移量和层差，主要用于赋值语句中事先记录左边变量的偏移量和层差，让yytext扫完后面的表达式后不致移回致使死循环

	typedef struct instruction{
		int f; //功能码
		int l; //层次差
		int a; //因指令而异
	}instruction;
	struct instruction code[CODESIZE]; //指令表

	typedef struct Symbols{
		int type; //定义的符号表中的类，取值为TCONST，TVAR，TPROC，TARRAY
		char name[NAMESIZE]; //常量或变量名
		int value; //常量或变量的值
		int level; //变量所在层次
		int offset; //偏移量，一般为3，0为静态链，1为动态链，2为返回地址
		int entry; //过程入口
	}Symbols;
	struct Symbols symbols[SYMBOLSIZE]; //符号表
	
%}

%token		IF
%token		THEN
%token		ELSE
%token		WHILE
%token		DO
%token		READ
%token		WRITE
%token		CALL
%token		BEGIN
%token		END
%token		CONST
%token		VAR
%token		PROCEDURE
%token		ODD
%token		INTEGER
%token		VARIABLE
%token		EQUAL
%token		NL
%token		NG
%token		REPEAT
%token		UNTIL
%token		SEMICOLON
%left		'+','-'
%left		'*','/'
%nonassoc UMINUS
%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE

%%

Program /*程序*/
		:	Init Block '.' { gen(OPR,0,0); }
		;

Block /*分程序*/
		:	SetDefaultOffset ConstantDeclaration VariableDeclaration ProcedureDeclaration BackfillBlock InitOffset Statement {  }
		;

ConstantDeclaration /*常量说明部分*/
		:	CONST ConstantStatement SEMICOLON	{  }
		|
		;

ConstantStatement /*常量定义部分*/
		:	VARIABLE EQUAL INTEGER	{ AddSymbol(TCONST, $3);}
		|	ConstantStatement ',' VARIABLE EQUAL INTEGER	{ AddSymbol(TCONST, $5); }
		| 
		;		

VariableDeclaration /*变量说明部分*/
		:	VAR VariableStatement SEMICOLON	{ }
		|
		;

VariableStatement /*变量定义部分*/
		:	Identifier	{  }
		|	VariableStatement ',' Identifier	{  }
		;
		
Identifier /*变量*/
		:	VARIABLE	{ AddSymbol(TVAR,-1); }
		/*|	VARIABLE '(' INTEGER ':' INTEGER ')'	{ AddArray($3,$5); }*/
		;

ProcedureHeader /*过程首部*/
		:	PROCEDURE VARIABLE SEMICOLON { 
				AddSymbol(TPROC,-1); //过程变量不需要赋值，故value设为-1
				disp[++PreLevel] = Top;
				offset[PreLevel] = 3;
			}
		;
		
ProcedureDeclaration /*过程说明部分*/
		:	ProcedureStatement SEMICOLON { }
		|
		;
		
ProcedureStatement /*过程定义部分*/ 
		:	ProcedureHeader GenCodeProc1 ProBlock { }
		|	ProcedureStatement SEMICOLON ProcedureHeader GenCodeProc1 ProBlock	{ }	
		;

ProBlock /*过程嵌套中的分程序，由于主程序到分程序只需要回填主程序的入口，故需要加以区分*/
		:	ConstantDeclaration VariableDeclaration ProcedureDeclaration Backfill InitOffset Statement GenCodeProc2	{}
		;

Statement /*语句*/
		:	AssignmentStatement	{  }
		|	CompoundStatement	{  }
		|	ConditionalStatement	{  }
		|	WhileDoStatement	{  }
		|	CallStatement	{  }
		|	ReadStatement	{  }
		|	WriteStatement	{  }
		;

		
Statements /*语句复数*/
		:	Statement	{  }
		|	Statements SEMICOLON Statement	{  }
		;
		
AssignmentStatement /*赋值语句*/
		:	VARIABLE GetAssign EQUAL Expression { gen(STO,l,a);}
		;

CompoundStatement /*复合语句*/
		:	BEGIN Statements END	{ }
		;
		
Condition /*条件*/
		:	Expression '#' Expression	{ gen(OPR,0,9); }
		|	Expression '>' Expression	{ gen(OPR,0,12); }
		|	Expression '<' Expression	{ gen(OPR,0,10); }
		|	Expression NG Expression	{ gen(OPR,0,13); }
		|	Expression NL Expression	{ gen(OPR,0,11); }
		|	Expression '=' Expression	{ gen(OPR,0,8); }
		|	ODD Expression				{ gen(OPR,0,6); }
		;

ConditionalStatement /*条件语句*/
		:	IF Condition GenCode1 THEN Statement Backfill1 	{  }
		|	IF Condition GenCode1 THEN Statement ELSE GenCode2 Backfill1 Statement Backfill2	{  }
		;
		
Expression /*表达式*/
		:	Term	{  }
		|	Expression '+' Expression	{ gen(OPR,0,2); }
		|	Expression '-' Expression	{ gen(OPR,0,3); };
		|	'-' Expression %prec UMINUS	{ gen(OPR,0,1); }
		|	'+' Expression %prec UMINUS	{  }
		;
		
Expressions /*表达式复数*/
		:	Expression	{ }
		|	Expressions ',' Expression	{  }
		;
				
Term /*项*/ 
		:	Factor	{  }
		|	Term '*' Term	{ gen(OPR,0,4); }
		|	Term '/' Term	{ gen(OPR,0,5); }
		;

Factor /*因子*/
		:	INTEGER	{ gen(LIT,0,$1); }
		|	VARIABLE { 
				Pos = find(TVAR);
				if (Pos>=0)
					gen(LOD,PreLevel - symbols[Pos].level,symbols[Pos].offset);
				else {
						Pos = find(TCONST);
						gen(LIT,0,symbols[Pos].value);
				} 
			}
		|	'(' Expression ')' {  }
		;
	
WhileDoStatement /*当型循环语句*/
		:	WHILE Backfill3 Condition GenCode1 DO Statement GenCodeWhile Backfill1	{  }
		|	REPEAT Backfill3 Statements UNTIL Condition GenCodeUntil	{  }
		;

CallStatement /*过程调用语句*/
		:	CALL VARIABLE	{ 
				Pos = find(TPROC);
				gen(CAL,PreLevel-symbols[Pos].level,symbols[Pos].entry);
				}
		;

ReadStatement /*读语句*/
		:	READ '(' VARIABLE ')'	{ 
				gen(OPR,0,16);
				Pos = find(TVAR);
				gen(STO,PreLevel - symbols[Pos].level,symbols[Pos].offset);
			}
		;
		
WriteStatement /*写语句*/
		:	WRITE '(' Expressions ')'	{ gen(OPR,0,14);gen(OPR,0,15);}
		;

/*以下操作用移进归约关系，仅仅是出于执行顺序的方便，跟文法无关*/

Init /*初始化，生成转向主程序入口的语句*/
		:	{
				CodeNo=0;
				PreLevel=0;
				Top=0;
				disp[PreLevel]=0;
				gen(JMP,0,0);
				Back1=0;
				Back2=0;
				Back3=0;
			}
		;

SetDefaultOffset /*将变量空间大小设为3（默认情况） */
		:	{ offset[PreLevel] = 3; }
		;
		
InitOffset /*开辟变量空间 */
		:	{ gen(INT,0,offset[PreLevel]); }
		;
		
BackfillBlock  /*回填主程序的跳转地址*/
		:	{ code[0].a = CodeNo; }
		;

GetAssign /*赋值语句时事先将等号左边的变量的偏移量和层差存入l和a*/
		:	{ 
				Pos = find(TVAR);
				l = PreLevel - symbols[Pos].level;
				a = symbols[Pos].offset; 
			}
		;

GenCodeProc1 /*生成过程的条件跳转pcode及填写*/
		:	{gen(JMP,0,0); BackStack[Back++] = CodeNo-1;}
		;

Backfill
		:	{ code[BackStack[--Back]].a = CodeNo; }
		;
		
GenCodeProc2 /*过程结束的退栈*/
		:	{
				gen(OPR,0,0);
				Top = disp[PreLevel];
				PreLevel--;
			}
		;

GenCode1 /*生成if的条件跳转pcode*/
		:	{ gen(JPC,0,0); BackStack1[Back1++] = CodeNo-1; }
		;
		
Backfill1 /*回填if的条件跳转位置*/
		:	{ code[BackStack1[--Back1]].a = CodeNo; }
		;

GenCode2 /*生成then部分编译结束而else尚未编译的时的无条件跳转pcode*/
		:	{ gen(JMP,0,0); BackStack2[Back2++] = CodeNo-1; }
		;
Backfill2 /*回填的then部分编译结束而else尚未编译的时的无条件跳转位置*/
		:	{ code[BackStack2[--Back2]].a = CodeNo; }
		;

GenCodeWhile /*生成while——do循环的无条件跳转pcode*/ 
		:	{ gen(JMP,0,BackStack3[--Back3]); }
		;
		
GenCodeUntil /*生成repeat——until循环的条件跳转pcode*/
		:	{ gen(JPC,0,BackStack3[--Back3]); }
		;
		
Backfill3 /*记录whiledo循环的条件跳转位置*/ 
		:	{ BackStack3[Back3++] = CodeNo;}
		;
		
%%

void yyerror( char * s ){
	printf( "parse error" );
}

void gen (int f,int l, int a) {/*产生三元式目标代码*/
	code[CodeNo].f = f;
	code[CodeNo].l = l;
	code[CodeNo].a = a;
	CodeNo++;
}

int find(int type){ /*在符号表中寻找元素的位置*/
	int l = PreLevel, i = Top-1;
	while (l >= 0){
		while (i >= disp[l]){
			if (strcmp(symbols[i].name,name)==0 && symbols[i].type==type)
				return i;
			i--;
		}
		l--;
	}
	return -1;
}

int check () { /*检查变量在符号表中是否重复*/
	int i;
	for(i=disp[PreLevel];i<Top;i++) {
		if(strcmp(symbols[i].name,name)==0)
		{
			return i;
		}
	}
	return -1;
}

void AddSymbol(int type, int value) { /*将非数组变量登入符号表*/
	if (check() != -1)
		printf("Error : Redefined!\n");
	switch(type) {
		case TCONST: { //常量
			strcpy(symbols[Top].name,name);
			symbols[Top].type = type;
			symbols[Top].value = value;
			Top++;
			break;
			}
		case TVAR: { //变量
			strcpy(symbols[Top].name,name);
			symbols[Top].type = type;
			symbols[Top].offset = offset[PreLevel];
			symbols[Top].level = PreLevel;
			offset[PreLevel]++;
			Top++;
			break;
			}
		case TPROC: { //过程
			strcpy(symbols[Top].name,name);
			symbols[Top].type = type;
			symbols[Top].level = PreLevel;
			symbols[Top].entry = CodeNo;
			Top++;
			break;
			}
		default:break;
	}
}

/*将非数组变量登入符号表，由于便于过程无法计算下表变量的值，
无法确定其在符号栈的位置，故PCODE无法实现，暂时略去*/
void AddArray(int begin,int end) { 
	/*if (check() != -1) {
		printf("Error : Redifined!\n");
		return;
	}
	}*/
}

int base(int l,int* s,int b) { //需找当前层的基址，对应interpret中的函数
	//l表示层差，s表示运行栈，b表示当前层基址
	if (l<0){
		printf("Error : Unknown Base!\n");
		return -1;
	}
	while (l>0){ //若l>0,顺着静态链往回找
		b = s[b];
		l--;
	}
	if(l==0)
		return b;
}

void fprintcode() { //将输出代码写入文件
	int i;
	for (i=0;i<CodeNo;i++){
		switch (code[i].f){
			case LIT: fprintf(FOut,"LIT ");break;
			case LOD: fprintf(FOut,"LOD ");break;
			case STO: fprintf(FOut,"STO ");break;
			case CAL: fprintf(FOut,"CAL ");break;
			case INT: fprintf(FOut,"INT ");break;
			case JMP: fprintf(FOut,"JMP ");break;
			case JPC: fprintf(FOut,"JPC ");break;
			case OPR: fprintf(FOut,"OPR ");break;
			default:break;
		}
		fprintf(FOut,"%d %d\n",code[i].l,code[i].a);
	}
}

void interpret() 
{
    int p, b, t;
    instruction i;  //instruction （指令）的类型定义（包括三个域f,l,a），请自行加入到头文件中，供其他文件共享
    int s[STACKSIZE];//stacksize：数据栈大小，需自己定义

    printf("start pl0\n");
    t=0; b=0;  //t：数据栈顶指针；b：基地址；
    p=0;	// 指令指针
    s[1]=0; s[2]=0; s[3]=0;
    do {
	
        i=code[p++];//code为指令存放数组，其定义请自行加入到头文件中，供其他文件共享
        switch (i.f) 
        {
        case LIT: 
            t=t+1;
            s[t]=i.a;
            break;
        case OPR: 
            switch(i.a) 
            {
                case 0:
                    t=b-1;
                    p=s[t+3];
                    b=s[t+2];
                    break;
                case 1: 
                    s[t]=-s[t];
                    break;
                case 2: 
                    t=t-1;
                    s[t]=s[t] + s[t+1];
                    break;
                case 3:
                    t=t-1;
                    s[t]=s[t] - s[t+1];
                    break;
                case 4: 
                    t=t-1;
                    s[t]=s[t] * s[t+1];
                    break;
                case 5: 
                    t=t-1;
                    s[t]=s[t] / s[t+1];
                    break;
                case 6: 
                    s[t]=(s[t] % 2 == 1);
                    break;
                case 8: 
                    t=t-1;
					
                    s[t]=(s[t] == s[t+1]);
                    break;
                case 9:
                    t=t-1;
                    s[t]=(s[t] != s[t+1]);
                    break;
                case 10:
                    t=t-1;
                    s[t]=(s[t]<s[t+1]);
                    break;
                case 11: 
                    t=t-1;
                    s[t]=(s[t]>=s[t+1]);
                    break;
                case 12: 
                    t=t-1;
                    s[t]=(s[t]>s[t+1]);
                    break;
                case 13: 
                    t=t-1;
                    s[t]=(s[t]<=s[t+1]);
                    break;
                case 14: 
                    printf(" %d", s[t]);
                    t=t-1;
                    break;
                case 15: 
                    printf("\n");
                    break;
                case 16: 
                    t=t+1;
                    printf(" ? ");
                    scanf("%d", &s[t]);
                    break;
            }
		    break;
        case LOD: 
            t=t+1;
            s[t]=s[base(i.l, s, b)+i.a];
            break;
        case STO: 
            s[base(i.l, s, b)+i.a]=s[t];
            t=t-1;
            break;
        case CAL:
            s[t+1]=base(i.l, s, b);
            s[t+2]=b;
            s[t+3]=p;
            b=t+1;
            p=i.a;
            break;
        case INT: 
            t=t+i.a;
            break;
        case JMP: 
            p=i.a;
            break;
        case JPC: 
            if (s[t]==0) 
            {
                p=i.a;
            }
			//printf("---nextp %d  ----\n",p);
            t=t-1;
            break;
        }//end switch
    }while (p!=0);
}

int main ( int argc, char *argv[] )
{
	int i;
	char output[100];
	switch(argc)
	{
	case 1: break;		
	case 2: 			//打开指定文件
		if ( ( fIn = fopen( argv[ 1 ], "r") ) == NULL ) {
	            printf( "File %s not found.\n", argv[ 1 ] );
	            exit( 1 );
	        }
	        else {
			yyin=fIn;
			strcpy(output,argv[1]);
			output[strlen(output)-4]='\0';
			strcat(output,".opcode");
			}
		break;
	default:
		printf("useage:flex [filename]\n");
		exit(1);
	}

	FOut = fopen(output,"w");
//	yylex();             //调用yylex()函数，进行词法分析
	
	yyparse();
	fprintcode(FOut);
	fclose(fIn);
	fclose(FOut);
	interpret();
	return 0;
}
