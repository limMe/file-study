# define	IF	257
# define	THEN	258
# define	ELSE	259
# define	WHILE	260
# define	DO	261
# define	READ	262
# define	WRITE	263
# define	CALL	264
# define	BEGIN	265
# define	END	266
# define	CONST	267
# define	VAR	268
# define	PROCEDURE	269
# define	ODD	270
# define	INTEGER	271
# define	VARIABLE	272
# define	EQUAL	273
# define	NL	274
# define	NG	275
# define	REPEAT	276
# define	UNTIL	277
# define	SEMICOLON	278
# define	UMINUS	279
# define	LOWER_THAN_ELSE	280

#define LIT 513
#define LOD 514
#define STO 515
#define CAL 516
#define INT 517
#define JMP 518
#define JPC 519
#define OPR 520

#define TCONST 521
#define TVAR 522
#define TPROC 523
#define TARRAY 524

#define CODESIZE 512  //三元式长度
#define SYMBOLSIZE 512  //符号表长度
#define DISPSIZE 512  //display表长度
#define STACKSIZE 512  //栈长度
#define NAMESIZE 64  //变量名长度