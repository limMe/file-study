%{
	#include <stdlib.h>
	#include <stdio.h>
	#include <malloc.h> 
	#include <memory.h>
	#include <string.h>
	#include "PLtoPC.tab.h"
	extern int yylval;
	extern char name[NAMESIZE];
%}

%% 
if							{ return IF; }
then						{ return THEN; }
else						{ return ELSE; }
while						{ return WHILE; }
do							{ return DO; }
repeat						{ return REPEAT; }
([\;]*)([ \t\n]*)until		{ return UNTIL; }
read						{ return READ; }
write						{ return WRITE; }
call						{ return CALL; }
begin						{ return BEGIN; }
([\;]*)([ \t\n]*)end		{ return END; }
const						{ return CONST; }
var							{ return VAR; }
procedure					{ return PROCEDURE; }
odd		 					{ return ODD; }
[0-9]+						{ yylval = atoi(yytext);return INTEGER; } /*整数*/
[A-Za-z][A-Za-z0-9]*		{ strcpy(name,yytext); return VARIABLE; } /*变量*/
\,		{ return ','; }
\#		{ return '#'; }
:=		{ return EQUAL; }
[\>][\=]		{ return NL; }
[\<][\=]		{ return NG; }
\=		{ return '='; }
\+		{ return '+'; }
\-		{ return '-'; }
\*		{ return '*'; }
\/		{ return '/'; }
\(		{ return '('; }
\)		{ return ')'; }
\<		{ return '<'; }
\>		{ return '>'; }
\.		{ return '.'; }
([\;]+)		{ return SEMICOLON; } /*分号*/
[\n\t\ ]		
.		{}
%%

int yywrap() {
	return 1;
}