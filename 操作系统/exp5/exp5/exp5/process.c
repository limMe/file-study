//
//  process.c
//  exp5
//
//  Created by zhongdian on 15/6/14.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#include "process.h"


#define NUM 5
typedef struct service{
    int name;
    int arriveTime;
    int serviceTime;
    int doneTime;
    int waitTime;
} service ;
int scheduleTime = 0;
int runTime = 0;
struct service Process[NUM];

void godDamnFunc(int a, int name, int service, int done){
    printf("%d, process %d is served, total time:%d, end time:%d.\n",a,name,service,done);
    runTime++;
}

void FCFS(){
    int i,j;
    for(i=0;i<NUM;i++){
        while(Process[i].arriveTime>runTime){
            printf("%d, no process.\n",runTime);
            runTime++;
        }
        for(j=0;j<Process[i].serviceTime;j++)
            godDamnFunc(runTime, Process[i].name, Process[i].serviceTime, j);
        printf("process %d finish.\n",Process[i].name);
    }
}

void RR(){
    int i,j,wait,flag;
    for(i=0;i<scheduleTime;i++){
        flag = -1;
        wait = 0;
        for(j=0;j<NUM;j++){
            if(Process[j].arriveTime<=i && Process[j].waitTime>=wait && Process[j].doneTime<Process[j].serviceTime){
                wait = Process[j].waitTime;
                flag = j;
                //printf("flag == %d.wait == %d.\n",flag,wait);
            }
        }
        if(flag == -1){
            printf("%d, no process.\n",i);
            continue;
        }
        godDamnFunc(i, Process[flag].name, Process[flag].serviceTime, Process[flag].doneTime);
        Process[flag].doneTime++;
        if(Process[flag].doneTime == Process[flag].serviceTime)
            printf("Process %d finish.\n",Process[flag].name);
        Process[flag].waitTime = 0;
        for(j=0;j<NUM;j++){
            if(j != flag && Process[j].arriveTime<=i &&Process[j].doneTime<Process[j].serviceTime)
                Process[j].waitTime++;
        }
    }
}
void SPN(){
    int i,j,flag,service;
    for(i=0;i<scheduleTime;){
        flag = -1;
        service = 100;
        for(j=0;j<NUM;j++){
            if(Process[j].arriveTime<=i && Process[j].serviceTime<=service && Process[j].doneTime<Process[j].serviceTime){
                service = Process[j].serviceTime;
                flag = j;
            }
        }
        if(flag == -1){
            printf("%d, no process.\n",i);
            i++;
            continue;
        }
        while(Process[flag].doneTime<Process[flag].serviceTime){
            godDamnFunc(i, Process[flag].name, Process[flag].serviceTime, Process[flag].doneTime);
            Process[flag].doneTime++;
            i++;
        }
        printf("Process %d finish.\n",Process[flag].name);
    }
}
void SRT(){
    int i,j,flag,minLeft;
    for(i=0;i<scheduleTime;i++){
        flag = -1;
        minLeft = 100;
        for(j=0;j<NUM;j++){
            if(Process[j].arriveTime<=i && (Process[j].serviceTime-Process[j].doneTime)<minLeft && Process[j].doneTime<Process[j].serviceTime){
                minLeft = Process[j].serviceTime-Process[j].doneTime;
                flag = j;
            }
        }
        if(flag == -1){
            printf("%d, no process.\n",i);
            continue;
        }
        godDamnFunc(i, Process[flag].name, Process[flag].serviceTime, Process[flag].doneTime);
        Process[flag].doneTime++;
        if(Process[flag].serviceTime == Process[flag].doneTime)
            printf("Process %d finish.\n",Process[flag].name);
    }
    
}
void HRRN(){
    int i,j,flag;
    double temp, r;
    for(i=0;i<scheduleTime;){
        flag = -1;
        r = 0.0;
        for(j=0;j<NUM;j++){
            temp = (i-Process[j].arriveTime+Process[j].serviceTime)/Process[j].serviceTime;
            if(Process[j].arriveTime<=i && temp>=r && Process[j].doneTime<Process[j].serviceTime){
                r = temp;
                flag = j;
            }
        }
        if(flag == -1){
            printf("%d, no process.\n",i);
            i++;
            continue;
        }
        while(Process[flag].doneTime<Process[flag].serviceTime){
            godDamnFunc(i, Process[flag].name, Process[flag].serviceTime, Process[flag].doneTime);
            Process[flag].doneTime++;
            i++;
        }
        printf("Process %d finish.\n",Process[flag].name);
    }
}

void cleardata(){
    int i;
    runTime = 0;
    for(i=0;i<NUM;i++){
        Process[i].doneTime = 0;
        Process[i].waitTime = 0;
    }
}
void process_init(){
    int a;
    for(a=1;a<6;a++){
        Process[a-1].name=a;
    }
    Process[0].arriveTime=0;
    Process[0].serviceTime=3;
    Process[1].arriveTime=2;
    Process[1].serviceTime=6;
    Process[2].arriveTime=4;
    Process[2].serviceTime=4;
    Process[3].arriveTime=6;
    Process[3].serviceTime=5;
    Process[4].arriveTime=8;
    Process[4].serviceTime=2;
    scheduleTime=20;
}
int main()
{
    int i;
    process_init();
    printf("这是一个图形界面\n");
    printf("       ████████████　　　　　　　 \n　　　██　　　◥██◤　　██　　　　 \n　◢███　　　　◥◤　　　██◣ \n　▊▎██◣　　　　　　　◢█▊ ▊　　　 \n　▊▎██◤　●　　●　  ◥█▊ ▊ \n  ▊　██　　　　　　　　　█▊ ▊ 　　 \n　◥▇██　▊　　　　　 ▊ █▇◤ \n　　　██　◥▆▄▄▄▆◤　█▊　　　◢▇▇◣ \n◢██◥◥▆▅▄▂▂▂▄▅▆███◣　▊◢╳█ \n█╳　　　　　　　　　　　　　　　╳█◥◤◢◤ \n █◣　　　⊕　　　　　⊕　　　  ◢█◤ ◢◤　 \n　　▊　　 　　 █ ◢◤　 　　 ╳▊╳ ◢◤ \n　　▊　　 　　　　　 　 　　　▊╳◢◤ \n　　▊　　　 　⊕　⊕　　　　█▇◤　　　 \n　◢█▇▆▆▆▅▅▅▆▆▆▇█◣　　　　　　　 \n　▊　▂　▊　　　　　　▊　▂　▊　　　　　　　 \n　◥▆▆▆◤　　　　　　◥▆▆▆◤\n");
    printf("您要使用何种进程调度算法？\n");
    printf("1.先来先服务\n2.时间片轮转\n3.短作业优先\n4.最短剩余时间优先\n5.最高响应比优先\nOTHER.不玩了\n");
    scanf("%d",&i);
   	printf("\n");
    switch(i){
        case 1: FCFS();break;
        case 2: RR();break;
        case 3: SPN();break;
        case 4: SRT();break;
        case 5: HRRN();break;
        default: printf("Oops!\n");
   	}
    cleardata();
    return 0;
}
