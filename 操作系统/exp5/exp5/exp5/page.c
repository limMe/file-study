//
//  page.c
//  exp5
//
//  Created by zhongdian on 15/6/14.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#include "page.h"
#define PAGE_MAX 3

int page_list[]={2,3,2,1,5,2,4,5,3,2,5,2};
int page_ali[PAGE_MAX]={0};
int zclock=0;

void page_print(){
    int a=0;
    for(a=0;a<PAGE_MAX;a++){
        if(page_ali[a]!=0)
            printf("%d|",page_ali[a]);
        else
            printf(" |");
    }
    printf("\n");
}

int page_best(int s,int i){
    int a,d,f,g,far;
    for(a=0;a<PAGE_MAX;a++){
        if(page_ali[a]==i)
            return 0;
        else if(page_ali[a]==0){
            page_ali[a]=i;
            return 0;
        }
    }
    d=far=0;
    for(a=0;a<PAGE_MAX;a++){
        f=11;
        for(g=s;g<12;g++)
            if(page_list[g]==page_ali[a]){
                f=g;
                break;
            }
        if(far<f){
            d=a;
            far=f;
        }
    }
    page_ali[d]=i;
    return 0;
}

int page_nearest(int s,int i){
    int a,d,f,g,far;
    for(a=0;a<PAGE_MAX;a++){
        if(page_ali[a]==i)
            return 0;
        else if(page_ali[a]==0){
            page_ali[a]=i;
            return 0;
        }
    }
    d=0;
    far=11;
    for(a=0;a<PAGE_MAX;a++){
        f=11;
        for(g=s;g>0;g--)
            if(page_list[g]==page_ali[a]){
                f=g;
                break;
            }
        if(far>f){
            far=f;
            d=a;
        }
    }
    page_ali[d]=i;
    return 0;
}

int page_zclock(int s,int i){
    int a;
    for(a=0;a<PAGE_MAX;a++){
        if(page_ali[a]%10==i){
            page_ali[a]=page_ali[a]%10+10;
            return 0;
        }
        else if(page_ali[a]==0){
            page_ali[a]=i+10;
            zclock=(a+1)%PAGE_MAX;
            return 0;
        }
    }
    for(a=0;a<=PAGE_MAX;a++){
        if(page_ali[zclock]<10){
            page_ali[zclock]=i+10;
            zclock=(zclock+1)%PAGE_MAX;
            break;
        }
        if(page_ali[zclock]>10){
            page_ali[zclock]-=10;
            zclock=(zclock+1)%PAGE_MAX;
        }
    }
    return 0;
}

int main1(){
    int i,a,s;
    printf("这是一个图形界面\n");
    printf("       ████████████　　　　　　　 \n　　　██　　　◥██◤　　██　　　　 \n　◢███　　　　◥◤　　　██◣ \n　▊▎██◣　　　　　　　◢█▊ ▊　　　 \n　▊▎██◤　●　　●　  ◥█▊ ▊ \n  ▊　██　　　　　　　　　█▊ ▊ 　　 \n　◥▇██　▊　　　　　 ▊ █▇◤ \n　　　██　◥▆▄▄▄▆◤　█▊　　　◢▇▇◣ \n◢██◥◥▆▅▄▂▂▂▄▅▆███◣　▊◢╳█ \n█╳　　　　　　　　　　　　　　　╳█◥◤◢◤ \n █◣　　　⊕　　　　　⊕　　　  ◢█◤ ◢◤　 \n　　▊　　 　　 █ ◢◤　 　　 ╳▊╳ ◢◤ \n　　▊　　 　　　　　 　 　　　▊╳◢◤ \n　　▊　　　 　⊕　⊕　　　　█▇◤　　　 \n　◢█▇▆▆▆▅▅▅▆▆▆▇█◣　　　　　　　 \n　▊　▂　▊　　　　　　▊　▂　▊　　　　　　　 \n　◥▆▆▆◤　　　　　　◥▆▆▆◤\n");
    printf("您要使用何种页面置换算法？\n");
    printf("1.最佳\n2.最近最少使用\n3.时钟\n");
    scanf("%d",&a);
    for(s=0;s<12;s++){
        i=page_list[s];
        switch(a){
            case 1:
                page_best(s,i);
                break;
            case 2:
                page_nearest(s,i);
                break;
            case 3:
                page_zclock(s,i);
                break;
            defualt:
                break;
        }
        page_print();
    }
    return 0;
}

