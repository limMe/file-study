/*程序说明：父子进程通过管道实现通信，父进程写，子进程读
  作者:hyc
  时间：2015.5.14
*/
#include <stdio.h>
#include <sys/types.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>

char parent[] = "A message to pipe' communication.\n";

void main()
{
	int pid,fd[2];
	char buf[100];
	pipe(fd);
	pid = fork();
	if (pid < 0)
	{
		printf("to create child error\n");
		exit(1);
	}
	if (pid > 0)
	{
		close(fd[0]);	/*父进程关闭读*/
		printf("parent process sends a message to child\n");
		write(fd[1],parent,sizeof(parent));/*父进程写*/
		close(fd[1]);	/*父进程关闭写*/
		printf("parent process waits the child to terminates\n");
		wait(0);
		printf("parent process terminates\n");
	}
	else
	{
		close(fd[1]);	/*子进程关闭写*/
		read(fd[0],buf,100);	/*子进程读*/
		printf("The message read by child process from parent is:%s\n",buf);
		close(fd[0]);	/*子进程关闭读*/
		printf("child process terminates\n");
	}
}
