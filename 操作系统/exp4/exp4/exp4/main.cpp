//
//  main.cpp
//  exp4
//
//  Created by zhongdian on 15/5/29.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <ctype.h>
#include<string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>

/* message buffer for msgsnd and msgrcv calls */
struct msgbuf {
    long mtype; /* type of message */
    char mtext[1]; /* message text */
};

char papeMsg[] = "父进程通过管道发送的消息";

int goPape()
{
    int pid,fd[2];
    char buf[100];
    pipe(fd);
    
    //然而并没有什么luan用
    mknod("NodeName", 0, 0);
    
    pid = fork();
    if (pid < 0)
    {
        printf("无法创建子进程\n");
        exit(1);
    }
    if (pid > 0)
    {
        close(fd[0]);	/*父进程关闭读*/
        printf("父进程发送消息\n");
        write(fd[1],papeMsg,sizeof(papeMsg));/*父进程写*/
        close(fd[1]);	/*父进程关闭写*/
        printf("父进程等待子进程关闭\n");
        wait(0);
        printf("父进程关闭\n");
        return 0;
    }
    else
    {
        close(fd[1]);	/*子进程关闭写*/
        read(fd[0],buf,100);	/*子进程读*/
        printf("子进程读取到的消息是:%s\n",buf);
        close(fd[0]);	/*子进程关闭读*/
        printf("子进程关闭\n");
        return 1;
    }
}

int sndMsg()
{
    int queue_id;
    struct msgbuf *msg, *recv_msg;
    int rc;
    int msgsz;
    
    //创建一个消息队列
    queue_id = msgget(IPC_PRIVATE, IPC_CREAT | 0600);
    if(queue_id == -1){
        perror("main's msgget");
        exit(1);
    }
    printf("创建的消息id是:'%d'.\n",queue_id);
    
    //新建一个消息
    msg = (struct msgbuf*)malloc(sizeof(struct msgbuf)+strlen("hello,world"));
    msg->mtype = 1;
    strcpy(msg->mtext,"balabala");
    
    //发送消息
    rc = msgsnd(queue_id,msg,strlen(msg->mtext)+1,0);
    if(rc == -1){
        perror("msgsnd");
        exit(1);
    }
    
    free(msg);//释放消息占用的空间
    printf("消息已加入队列.\n");
    
    //接收消息
    //M.U 这里必须要知道strlen吗？
    recv_msg = (struct msgbuf*)malloc(sizeof(struct msgbuf)+strlen("balabala"));
    msgsz = strlen(recv_msg->mtext)+1;
    rc = msgrcv(queue_id,recv_msg,msgsz,0,0);
    if(rc == -1){
        perror("msgrcv");
        exit(1);
    }
    
    printf("收到的消息类型是 '%ld'\n内容是 '%s'\n",recv_msg->mtype,recv_msg->mtext);
    
    //删除消息队列
    msgctl(queue_id,IPC_RMID,NULL);
    return 0;
}

int main()
{
    printf("这是一个菜单：\n1).浓香鸡肉卷\n2).匈牙利火烩意面\n\n");
    int i;
    scanf("%d",&i);
    if (i == 1) {
        printf("========什么鸡肉卷，其实就是PIPE通讯========\n");
        if (goPape()==0) {
            printf("========================================\n");
        }
    }
    else if(i == 2)
    {
        printf("============没错，这里是消息队列============\n");
        sndMsg();
        printf("=========================================\n");
    }
    else
    {
        printf("你点的菜我这里没有");
    }
    return 0;
}
