/*程序说明：用消息队列实现自己给自己发消息
  作者:hyc
  时间：2015.5.14
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/ipc.h>
#define __USE_GNU
#include<sys/msg.h>

int main(int argc, char *agrv[])
{
	int queue_id;
	struct msgbuf *msg, *recv_msg;
	int rc;
	int msgsz;
	
	//创建一个消息队列
	queue_id = msgget(IPC_PRIVATE, IPC_CREAT | 0600);
	if(queue_id == -1){
		perror("main's msgget");
		exit(1);
	}
	printf("the created message's id='%d'.\n",queue_id);
	
	//新建一个消息，内容是"hello,world"
	msg = (struct msgbuf*)malloc(sizeof(struct msgbuf)+strlen("hello,world"));
	msg->mtype = 1;
	strcpy(msg->mtext,"hello,world");
	
	//发送消息
	rc = msgsnd(queue_id,msg,strlen(msg->mtext)+1,0);
	if(rc == -1){
		perror("msgsnd");
		exit(1);
	}
	
	free(msg);//释放消息占用的空间
	printf("message is placed on message's queue.\n");
	
	//接收消息
	recv_msg = (struct msgbuf*)malloc(sizeof(struct msgbuf)+strlen("hello,world"));
	msgsz = strlen(recv_msg->mtext)+1;
	rc = msgrcv(queue_id,recv_msg,msgsz,0,0);
	if(rc == -1){
		perror("msgrcv");
		exit(1);
	}
	
	printf("received message's mtype is '%d'; mtext is '%s'\n",recv_msg->mtype,recv_msg->mtext);
	
	//删除消息队列
	msgctl(queue_id,IPC_RMID,NULL);
}
