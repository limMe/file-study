#!/bin/bash

# Author : zhongdian
# Copyright (c) 1994 - 2015
# Script follows here:

if [[ $1 = "q" ]]; then
	result=$(ps $2 | wc -l)
	if [ $result = 1 ]; then
		echo Process $2 not found
	else 
		echo I have found process $2 here
		ps -axl | awk -v n=$2 'NR==1||$2==n{print}' > output.txt
		ps -axl | awk -v n=$2 'NR==1||$2==n{print}'
	fi

elif [[ $1 = "k" ]]; then

	kill $2
	if [ $? != 0 ]; then
		echo Kill process $2 failed
	else
		echo Successfully Kill process $2
	fi

	
else
	echo you could use Q-uery or K-ill as para to act this stuff
fi
