/* function: 
 * A demo of usage of fork.
 * author: hyc
 * date: 2015.04.08
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int glob = 3;
int main(void)
{
	pid_t pid;
	if ((pid = fork()) < 0)	//fork wrong
	{
		printf("fork()   error.\n");
		exit(0);
	}
	else if (pid == 0)	//child
	{
		printf("This is child!\n");
	}
	else if (pid > 0)	//parent
	{
		printf("This is parent!\n");
	}
}
