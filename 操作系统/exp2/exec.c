/* function: 
 * A demo of usage of execve, child process will execute "ls -l"
 * author: hyc
 * date: 2015.04.09
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

char *argv[ ]={"/bin/ls","-l",NULL};

int main(void)
{
	pid_t pid;
	if((pid = fork()) < 0)	//wrong
	{
		printf("fork error\n");
		exit(0);
	}
	else if(pid == 0)	//child
	{
		printf("--------------\n");
		printf("This is child!\n");
		if (execve("/bin/ls",argv,NULL) < 0)	//execute "ls -l"
			printf("execve error\n");
		printf("You will never say me!\n");
	}
	else			//parent
	{
		sleep(5);
		printf("--------------\n");
		printf("This is parent.\n");
	}
	return 0;
} 
