//
//  main.cpp
//  test
//
//  Created by zhongdian on 15/5/8.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

char *argv[ ]={"","lalala demaciya",NULL};

int main(void)
{
    pid_t pid;
    if((pid = fork()) < 0)	//fork失败一般是返回－1
    {
        printf("fork失败\n");
        exit(0);//退出当前进程
    }
    else if(pid == 0)	//pid=0时是子进程
    {
        printf("\n--------------\n");
        printf("CHILD PROGRESS\n");
        if (execve("/Users/zhongdian/Desktop/customer",argv,NULL) < 0)	//跑一个我放在桌面上的程序
            printf("execve失败\n");
        printf("Never showed!\n");
    }
    else			//父进程会返回当前pid
    {
        wait(0);//If I use 1 here, there will be an error
        printf("\n--------------\n");
        printf("FATHER PROGRESS\n");
    }
    return 0;
}