//
//  main.cpp
//  test
//
//  Created by zhongdian on 15/5/8.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include "Producer-consumer.h"
#include <iostream>
#include <time.h>
#include <unistd.h>


#define RAND_DIVISOR 5
#define TRUE 1

/* 互斥锁 */
pthread_mutex_t mutex;

/* 信号量 */
sem_t full, empty;

/* 缓存 */
buffer_item buffer[BUFFER_SIZE];

/* 缓存计数 */
int counter;

pthread_t tid;


void initializeData() {
    
    pthread_mutex_init(&mutex, NULL);
    
    sem_init(&full, 0, 1);
    
    sem_init(&empty, 0, BUFFER_SIZE);

    counter = 0;
}



int insert_item(buffer_item item) {

    if(counter < BUFFER_SIZE) {
        buffer[counter] = item;
        counter++;
        return 0;
    }
    else {
        return -1;
    }
}


int remove_item(buffer_item *item) {

    if(counter > 0) {
        *item = buffer[(counter-1)];
        counter--;
        return 0;
    }
    else {
        return -1;
    }
}


void *producer(void *param) {
    buffer_item item;
    
    while(TRUE) {
        int rNum = rand()%RAND_DIVISOR +1;
        sleep(rNum);
        
        item = rand();
        
        sem_wait(&empty);
        
        pthread_mutex_lock(&mutex);
        
        if(insert_item(item)) {
            fprintf(stderr, "---- 生产者冷脸热屁！----\n");
        }
        else {
            printf("生产者生产了地址在\t%d\t的缓存\n", item);
        }
    
        pthread_mutex_unlock(&mutex);
        
        sem_post(&full);
    }
}


void *consumer(void *param) {
    buffer_item item;
    
    while(TRUE) {
        int rNum = rand() % RAND_DIVISOR + 1;
        sleep(rNum);
        
        sem_wait(&full);

        pthread_mutex_lock(&mutex);
        if(remove_item(&item)) {
            fprintf(stderr, "---- 消费者空手而归！----\n");
        }
        else {
            printf("消费者消化了地址在\t%d\t的缓存\n", item);
        }

        pthread_mutex_unlock(&mutex);
        
        sem_post(&empty);
    }
}



int main(int argc, char *argv[]) {
    
    int i;
    
    int sleepTimeMain;
    printf("%s","输入主线程休眠的秒数");
    scanf("%d",&sleepTimeMain);
    int numProTs;
    printf("%s","输入生产者线程数");
    scanf("%d",&numProTs);
    int numConsTs;
    printf("%s","输入消费者线程数");
    scanf("%d",&numConsTs);
    
    initializeData();
    
    /* 创建生产者 */
    for(i = 0; i < numProTs; i++) {
        pthread_create(&tid,NULL,producer,NULL);
    }
    
    /* 创建消费者 */
    for(i = 0; i < numConsTs; i++) {
        pthread_create(&tid,NULL,consumer,NULL);
    }
    
    sleep(sleepTimeMain);
    
    /* Exit the program */
    printf("主线程结束\n");
    exit(0);
}