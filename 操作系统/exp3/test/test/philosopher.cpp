//
//  main.cpp
//  test
//
//  Created by zhongdian on 15/5/8.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define N 5
pthread_mutex_t chopsticks[N];

void * philosopher(void * arg)
{
    int i = *((int *) arg);
    if (i % 2)
    {
        pthread_mutex_lock (&chopsticks[(i+1)%N]);//获得右手的筷子
        sleep(rand()%10);
        pthread_mutex_lock (&chopsticks[i]);//获得左手的筷子
        printf ("philosopher%d is eating.\n", i);
        sleep(rand()%10);
        //释放左右两边的筷子
        pthread_mutex_unlock (&chopsticks[(i+1)%N]);
        sleep(rand()%10);
        pthread_mutex_unlock (&chopsticks[i]);
        printf ("philosopher%d is thinking.\n", i);
        sleep(rand()%10);
    }
    else
    {
        pthread_mutex_lock (&chopsticks[i]);//跟着我左手右手一个慢动作
        sleep(rand()%10);
        pthread_mutex_lock (&chopsticks[(i+1)%N]);//右手左手慢动作重播
        printf ("philosopher%d is eating.\n", i);
        sleep(rand()%10);
        //释放在左右两边的筷子
        pthread_mutex_unlock (&chopsticks[i]);
        sleep(rand()%10);
        pthread_mutex_unlock (&chopsticks[(i+1)%N]);
        printf ("philosopher%d is thinking.\n", i);
        sleep(rand()%10);
    }
    //返回了一个没有意义的东西
    return &i;
}

int main1 ()
{
    int i;
    pthread_t thread[N];
    int number[N];
    
    for (i = 0; i < N; i++)	//用一个线程来模拟一个哲学家
    {
        number[i] = i;
        pthread_create (&thread[i], NULL, philosopher, &number[i]);
    }
    for ( i = 0; i < N; i++)
    {
        pthread_join(thread[i], NULL);
    }
    return 0;
}