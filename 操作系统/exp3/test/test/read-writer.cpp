//
//  read-writer.cpp
//  test
//
//  Created by zhongdian on 15/5/15.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#include "read-writer.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define FILENAME "sharedFile.bin"

int main2(void){
    
    int i = 0;
    //int k = 0 ;
    int ret = 0;
    int *settings;
    int num_writers = 0;
    int num_readers = 0;
    int num_iterations = 0;
    //FILE *fp;
    
    int waiting_writers = 0;
    int waiting_readers = 0;
    
    settings = getSettings();
    num_readers = settings[0];
    num_writers = settings[1];
    num_iterations = settings[2];
    initializeFile(num_writers);
    
    thread_data writers_thread_data[num_writers];
    thread_data readers_thread_data[num_readers];
    pthread_t writers_thread[num_writers];
    pthread_t readers_thread[num_readers];
    pthread_mutex_t *temp_readers_lock = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) *num_readers);
    
    // 写者互斥锁.
    if(pthread_mutex_init(&writer_lock, NULL) != 0){
        printf("互斥锁错误\n");
        exit(1);
    }
    
    // 读者互斥锁.
    for(i=0; i <num_readers;i++){
        if(pthread_mutex_init(&readers_thread_data[i].my_lock, NULL) != 0){
            printf("互斥锁错误\n");
            exit(1);
        }
        temp_readers_lock[i] = readers_thread_data[i].my_lock;
    }
    
    // 读者线程.
    for(i = 0; i <num_readers; i++){
        readers_thread_data[i].thread_id = i;
        readers_thread_data[i].iterations = num_iterations;
        readers_thread_data[i].writers = num_writers;
        readers_thread_data[i].readers = num_readers;
        readers_thread_data[i].reader_locks = NULL;
        ret = pthread_create(&readers_thread[i], 0, readNumber, &readers_thread_data[i]);
        if(ret != 0){
            printf("创建线程错误!\n");
            exit(1);
        }
    }
    
    // 写者线程.
    for(i = 0; i <num_writers; i++){
        writers_thread_data[i].thread_id = i;
        writers_thread_data[i].iterations = num_iterations;
        writers_thread_data[i].writers = num_writers;
        writers_thread_data[i].readers = num_readers;
        writers_thread_data[i].reader_locks = temp_readers_lock;
        ret = pthread_create(&writers_thread[i], 0, increment, &writers_thread_data[i]);
        if(ret != 0){
            printf("创建线程失败!\n");
            exit(1);
        }
    }
    
    // 垃圾回收.
    for(i=0;i<num_writers;i++){
        pthread_join(writers_thread[i],NULL);
    }
    for(i=0;i<num_readers;i++){
        pthread_join(readers_thread[i],NULL);
    }
    
    pthread_mutex_destroy(&writer_lock);
    for(i=0;i<num_readers;i++){
        pthread_mutex_destroy(&readers_thread_data[i].my_lock);
    }
    pthread_mutex_destroy(temp_readers_lock);
    free(temp_readers_lock);
    printf("%s","任务完成");
    pthread_exit(NULL);
    return 0;
}

int* getSettings(){
    static int input[3];
    printf("\n读者写者问题.\n");
    printf("输入读者的数目 ");
    scanf("%d", &input[0]);
    printf("输入写者的数目 ");
    scanf("%d", &input[1]);
    printf("输入执行的轮数 ");
    scanf("%d", &input[2]);
    printf("创建了 %d个读者进程  %d个写者进程 总共搞%d轮\n", input[0], input[1], input[2]);
    return input;
}

void* initializeFile(int num_writers){
    int fill = 0;
    int i = 0;
    FILE *fp;
    fp =fopen(FILENAME,"w+");
    
    if(fp != NULL){
        for(i = 0; i < num_writers; i++){
            fwrite(&fill, sizeof(int), 1, fp);
        }
        fclose(fp);
    }else{
        printf("Could not open file.\n");
        exit(1);
    }
    return &i;
}

void* increment(void* parameter){
    thread_data * cur_thread;
    FILE *fp;
    cur_thread = (thread_data *)parameter;
    int value = 0;
    int i;
    int k;
    
    for(k=1;k<=cur_thread->iterations;k++){
        for(i=0;i<cur_thread->readers;i++){
            pthread_mutex_lock(&cur_thread->reader_locks[i]);
        }
        pthread_mutex_lock(&writer_lock);
        fp = fopen(FILENAME,"rb+");
        
        for( i = 0; i < cur_thread->writers; i++){
            fseek(fp,sizeof(int)*i,SEEK_SET);
            fread(&value, sizeof(int), 1, fp);
            
            if( i == cur_thread->thread_id){
                value++;
                fseek(fp,sizeof(int)*i,SEEK_SET);
                fwrite(&value, sizeof(int), 1, fp);
            }
        }
        printf("%d号写者正在写%s",cur_thread->thread_id+1,"正在写\n");
        fclose(fp);
        pthread_mutex_unlock(&writer_lock);
        for(i=0;i<cur_thread->readers;i++){
            pthread_mutex_unlock(&cur_thread->reader_locks[i]);
        }
        usleep(rand()%1000);
    }
    return &i;
}

void* readNumber(void* parameter){
    thread_data * cur_thread;
    cur_thread = (thread_data *)parameter;
    int contents[cur_thread->writers];
    FILE *fp;
    char *contents_string = (char *)malloc(sizeof(int) *cur_thread->writers);
    char temp[sizeof(int)];
    int i;
    int k;
    for(k=1;k<=cur_thread->iterations;k++){
        pthread_mutex_lock(&cur_thread->my_lock);
        
        strcpy(contents_string,"");
        fp = fopen(FILENAME,"rb+");
        fread(contents, sizeof(int),cur_thread->writers, fp);
        for(i=0;i<cur_thread->writers;i++){
            sprintf(temp, "%d ",contents[i]);
            strcat(contents_string,temp);
            
        }
        fclose(fp);
        printf("第%d轮: 读者%d 看到的是 %s \n",k,cur_thread->thread_id+1, contents_string);
        fflush(stdout);
        pthread_mutex_unlock(&cur_thread->my_lock);
        
        usleep(rand()%1000);
        
    }
    free(contents_string);
    return &i;
}