//
//  read-writer.h
//  test
//
//  Created by zhongdian on 15/5/15.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#ifndef __test__read_writer__
#define __test__read_writer__

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

typedef struct {
    int thread_id;
    int iterations;
    int writers;
    int readers;
    pthread_mutex_t my_lock;
    pthread_mutex_t * reader_locks;
} thread_data;

pthread_mutex_t writer_lock;

void* increment(void* parameter);
void* readNumber(void* parameter);
void* initializeFile(int num_writers);
int* getSettings();


#endif /* defined(__test__read_writer__) */
