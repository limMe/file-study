/*程序说明：
  用多个线程实现哲学家问题,偶数编号的哲学家先获得左边的筷子，奇数编号的哲学家先获得右边的筷子
  作者:hyc
  时间：2014.10.26
*/

#include <stdio.h>
#include <pthread.h>

#define N 5
pthread_mutex_t chopsticks[N];

void * philosopher(void * arg)
{
	int i = *((int *) arg);
	if (i % 2)
	{
		pthread_mutex_lock (&chopsticks[i]);//获得左手的筷子
		sleep(1);
		pthread_mutex_lock (&chopsticks[(i+1)%N]);//获得右手的筷子
		printf ("philosopher%d is eating.\n", i);
		sleep(3);
		//释放左右两边的筷子
		pthread_mutex_unlock (&chopsticks[(i+1)%N]);
		sleep(1);
		pthread_mutex_unlock (&chopsticks[i]);
		printf ("philosopher%d is thinking.\n", i);
		sleep(3);
	}
	else
	{
		pthread_mutex_lock (&chopsticks[(i+1)%N]);//获得右手的筷子
		sleep(1);
		pthread_mutex_lock (&chopsticks[i]);//获得左手的筷子
		printf ("philosopher%d is eating.\n", i);
		sleep(3);
		//释放在左右两边的筷子
		pthread_mutex_unlock (&chopsticks[i]);
		sleep(1);
		pthread_mutex_unlock (&chopsticks[(i+1)%N]);
		printf ("philosopher%d is thinking.\n", i);
		sleep(3);
	}
}

int main ()
{
	int i;
	pthread_t thread[N];
	int number[N];

	for (i = 0; i < N; i++)	//用一个线程来模拟一个哲学家
	{
		number[i] = i;
		pthread_create (&thread[i], NULL, philosopher, &number[i]);
	}
	for ( i = 0; i < N; i++)
	{
		pthread_join(thread[i], NULL);
	}
	return 0;
}

